#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
        int (*add)(int a, int b);
        int (*sub)(int a, int b);
        int (*mul)(int a, int b);
        int (*div)(int a, int b);
} calc_t;

static calc_t *this;

calc_t *new_calc_t();

void free_calc();

int add(int a, int b);

int sub(int a, int b);

int multiply(int a, int b);

int divide(int a, int b);

calc_t *new_calc_t()
{
	this = malloc(sizeof(calc_t));
	
	if (NULL == this)
	{
		fprintf(stderr, "Out of memory.\n");
		exit(1);
	}
	this->add = add;
	this->sub = sub;
	this->mul = multiply;
	this->div = divide;
	atexit(free_calc);

	return this;
}

void free_calc()
{
	free(this);
}

int add(int a, int b)
{
	return a + b;
}

int sub(int a, int b)
{
	return a - b;
}

int multiply(int a, int b)
{
	return a * b;
}

int divide(int a, int b)
{
	return a / b;
}

