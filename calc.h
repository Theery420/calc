#ifndef __CALC_H__
#define __CALC_H__

typedef struct
{
 	int (*add)(int a, int b);       
        int (*sub)(int a, int b);
	int (*mul)(int a, int b);
	int (*div)(int a, int b);
} calc_t;

calc_t *new_calc_t();

#endif

