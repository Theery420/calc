
#include <stdio.h>
#include "calc.h"

int main()
{
	calc_t *calc = new_calc_t();
	printf("%d\n", calc->add(5, 3));
	printf("%d\n", calc->sub(5, 3));
	printf("%d\n", calc->mul(5, 3));
	printf("%d\n", calc->div(5, 3));

	return 0;
}

